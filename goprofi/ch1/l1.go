package ch1

import (
	"bufio"
	"fmt"
	"io"
	"log"
)

/*
#include <stdio.h>
void hwC() {
  printf("hello from ansi c \n");
}
*/
import "C"

type Rdfile struct {
}

func (r *Rdfile) ReadIN(in io.Reader) {
	newscan := bufio.NewScanner(in)
	for newscan.Scan() {
		word := newscan.Text()
		switch word {
		case "exit":
			log.Println("-- found exit command, exit now, cya ")
			return
		default:
			log.Println(word)
		}
	}
}

//run c code from application in golang
func RunCHW() {
	fmt.Println("hello world from Golang before call C function")
	C.hwC()
	fmt.Println("hello world from Golang after call C function")
}
