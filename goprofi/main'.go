package main

import "C"
import (
	"gitlab.com/Spouk/booksabooks/dbs/psql"
	"gitlab.com/Spouk/booksabooks/goprofi/ch1"
	"log"
	"os"
)

func main() {
	//call Ansi C function in Golang code
	ch1.RunCHW()

	//connection to database posgresql
	cu := psql.Connector{}
	cu.Connect(psql.ConnectInfo{
		Host:     "localhost",
		Port:     "5432",
		User:     "spouk",
		Password: "spoukru",
		Dbname:   "spoukru",
	})

	os.Exit(1)

	r := ch1.Rdfile{}
	//read os.stdin
	r.ReadIN(os.Stdin)
	//read file
	if fin, err := os.Open("/etc/passwd"); err != nil {
		log.Println(err)
		return
	} else {
		r.ReadIN(fin)
	}
}
