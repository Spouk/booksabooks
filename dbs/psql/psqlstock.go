package psql

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

type ConnectInfo struct {
	Host     string
	Port     string
	User     string
	Password string
	Dbname   string
}
type Testers struct {
	gorm.Model
	Name     string
	Password string
	Email    string
}
type Testers2 struct {
	gorm.Model
	Name     string
	Password string
	Email    string
}
type Session struct {
	gorm.Model
	Cook   string
	Userid int
}

type Connector struct {
	log *log.Logger
}

func (c *Connector) CheckError(err error) {
	if err != nil {
		log.Fatalf("%v", err)
	}
}
func (c *Connector) Connect(con ConnectInfo) {
	// connection string
	psqlconn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", con.Host, con.Port, con.User, con.Password, con.Dbname)

	// open database
	db, err := gorm.Open(postgres.Open(psqlconn), &gorm.Config{
		SkipDefaultTransaction:                   false,
		NamingStrategy:                           nil,
		FullSaveAssociations:                     false,
		Logger:                                   nil,
		NowFunc:                                  nil,
		DryRun:                                   false,
		PrepareStmt:                              false,
		DisableAutomaticPing:                     false,
		DisableForeignKeyConstraintWhenMigrating: false,
		DisableNestedTransaction:                 false,
		AllowGlobalUpdate:                        false,
		QueryFields:                              false,
		CreateBatchSize:                          0,
		ClauseBuilders:                           nil,
		ConnPool:                                 nil,
		Dialector:                                nil,
		Plugins:                                  nil,
	})
	c.CheckError(err)

	// check db
	c.CheckError(err)

	fmt.Println("Connected!")
	//create table
	result := db.AutoMigrate(&Testers2{})
	fmt.Printf("result create table:  %#v", result)

	result = db.AutoMigrate(&Session{})
	fmt.Printf("result create table session:  %#v", result)

	result = db.Create(&Testers{
		Model:    gorm.Model{},
		Name:     "testername",
		Password: "testerpassword",
		Email:    "test@test.com",
	}).Error
	fmt.Printf("result insert record:  %#v", result)
	result = db.Create(&Testers2{
		Model:    gorm.Model{},
		Name:     "testername",
		Password: "testerpassword",
		Email:    "test@test.com",
	}).Error
	fmt.Printf("result insert record:  %#v", result)
	db.Commit()
	ddd, _ := db.DB()
	err = ddd.Close()
	fmt.Printf("close connection to database => result: %v\n", err)
}
