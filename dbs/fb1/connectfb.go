package main

import (
	"database/sql"
	"fmt"
	_ "github.com/nakagami/firebirdsql"
	"log"
)

type dbs struct {
	con *sql.Conn
}
type customer struct {
	Customerid sql.NullInt32  `json:"customer_id"`
	Name       sql.NullString `json:"name"`
	Address    sql.NullString `json:"address"`
	Zipcode    sql.NullString `json:"zipcode"`
	Phone      sql.NullString `json:"phone"`
}

func main() {
	dsn := "sysdba:sysdba@localhost/examples"
	var n int
	conn, err := sql.Open("firebirdsql", dsn)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	conn.QueryRow("SELECT Count(*) FROM rdb$relations").Scan(&n)
	if rows, err := conn.Query("select first 100 * from customer"); err != nil {
		log.Println(err)
	} else {
		for rows.Next() {
			var cc customer
			if err := rows.Scan(&cc.Customerid, &cc.Name, &cc.Address, &cc.Zipcode, &cc.Phone); err != nil {
				log.Printf("error scan: %v", err)
			} else {
				log.Printf("result: %#v", cc)
			}

			if stock, err := rows.Columns(); err != nil {
				log.Printf("error read rows: %v", err)
			} else {
				log.Printf("result: %#v", stock)
			}
		}
	}

	fmt.Println("Relations count=", n)

}
